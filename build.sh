echo 'Fetching contentful data';
contentful_data=$(curl --location --request GET 'https://cdn.contentful.com/spaces/SPACE/environments/master/entries?access_token=TOKEN')
echo 'Printing contentful data';
echo "$contentful_data";