import * as React from "react"
import { graphql, useStaticQuery } from "gatsby"
import "../styling/index.css"

const IndexPage = () => {
  const assetsQuery = graphql`
    {
      contentfulHeader {
        contentful_id
        title
        body {
          body
        }
        url
      }
  }`

  const data = useStaticQuery(assetsQuery);
  return (
    <main>
      <h1>{data.contentfulHeader.title}</h1>
      <span>{data.contentfulHeader.body.body}</span>
      <img src={data.contentfulHeader.url}></img>
    </main>
  )
}

export default IndexPage
